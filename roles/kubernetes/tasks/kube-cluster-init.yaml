- name: ensure kubernetes dir
  ansible.builtin.file:
    path: /etc/kubernetes
    state: directory
    mode: 555
    recurse: no

- name: ensure config dir
  ansible.builtin.file:
    path: ${HOME}/.kube
    state: directory
    mode: 500
    recurse: no

- name: ensure kubelet env
  ansible.builtin.file:
    path: /etc/kubernetes/kubelet.env
    state: touch
    mode: u+rw,g-wx,o-wx
    modification_time: preserve
    access_time: preserve

- name: kubelet env cgroups -> systemd
  ansible.builtin.lineinfile:
    path: /etc/kubernetes/kubelet.env
    regexp: '^KUBELET_ARGS='
    line: 'KUBELET_ARGS=--cni-bin-dir=/opt/cni/bin,--cgroup-driver="systemd"'

# - name: kubelet config cgroups -> systemd
#   ansible.builtin.lineinfile:
#     path: /var/lib/kubelet/config.yaml
#     regexp: '^cgroupDriver:'
#     line: 'cgroupDriver: systemd'

# CRI patches and selection

- name: default registry set
  ansible.builtin.lineinfile:
    path: /etc/containers/registries.conf
    regexp: '^unqualified-search-registries'
    insertafter: '^#unqualified-search-registries'
    line: 'unqualified-search-registries = ["docker.io"]'

- name: ensure enable cri-o
  ansible.builtin.systemd:
    name: crio
    state: restarted
    enabled: yes

- name: ensure start kubelet
  ansible.builtin.systemd:
    name: kubelet
    state: started
    enabled: no

- name: download kubeadm images all together
  ansible.builtin.shell:
    cmd: /usr/bin/kubeadm config images pull

- name: generate certificate key ;c
  when: inventory_hostname in [groups["master"][0]]
  ansible.builtin.shell:
    cmd: "/usr/bin/kubeadm certs certificate-key"
  register: certificate_key

# KUBEADM CONFIG

- name: ensure kubeadm config dir
  ansible.builtin.file:
    path: "{{ kubeadm.config.dir }}"
    state: directory
    mode: 555
    recurse: no

- name: overwrite our custom kubeadm config
  when: inventory_hostname in [groups["master"][0]]
  ansible.builtin.template:
    src: "{{ kubeadm.config.src }}"
    dest: "{{ kubeadm.config.dest }}"
    trim_blocks: no
    owner: root
    group: root
    mode: '0644'
    backup: yes

# PRIMARY BOOTSTRAPPING KUBERNETES

- block:
  - name: Initialise master primary
    when: inventory_hostname in [groups["master"][0]]
    ansible.builtin.shell:
      cmd: "/usr/bin/kubeadm init --config={{ kubeadm.config.dest }} --upload-certs"
      # cmd: "/usr/bin/kubeadm init --config={{ kubeadm.config.dest }} --cri-socket=unix:///var/run/crio/crio.sock --control-plane-endpoint {{ lb_ip }}:{{ lb.port }} --apiserver-bind-port {{ kubeapi.port }} --upload-certs --pod-network-cidr {{ podNetworkCidr }} --certificate-key {{ certificate_key['stdout'] }}"
    register: kubeadm_output
  rescue:
  - name: kubeadm reset
    when: inventory_hostname in [groups["master"][0]]
    ansible.builtin.shell:
      cmd: /usr/bin/kubeadm reset -f

- name: verify control plane is live from nodes perspective
  ansible.builtin.shell:
    cmd: curl -k https://{{ hostvars[groups['master'][0]]['lb_ip'] }}:{{ lb.port }}/livez?verbose
  register: api_status

- name: display kubeapi status request
  when: inventory_hostname in [groups["master"][0]]
  ansible.builtin.debug:
    var: api_status["stdout"]

- name: Copy master config
  when: inventory_hostname in [groups["master"][0]]
  copy:
    src: /etc/kubernetes/admin.conf
    dest: ${HOME}/.kube/config
    remote_src: yes
    mode: '0600'

- name: display kubeadm output
  when: inventory_hostname in [groups["master"][0]]
  ansible.builtin.debug:
    var: kubeadm_output["stdout"]

# GETTING WORKER TOKENS

- name: get worker join command
  when: inventory_hostname in [groups["master"][0]]
  ansible.builtin.shell:
    cmd: "/usr/bin/kubeadm token create --print-join-command"
  register: worker_join
  until: worker_join.rc == 0
  retries: 20
  delay: 20

- name: extract worker token
  when: inventory_hostname in [groups["master"][0]]
  shell: |
    echo "{{ worker_join['stdout'] }}" | grep -o -P "(--token )\K\S+"
  check_mode: false
  changed_when: false
  register: token_worker

- name: extract worker token-cert-hash
  when: inventory_hostname in [groups["master"][0]]
  shell: |
    echo "{{ worker_join['stdout'] }}" | grep -o -P "(--discovery-token-ca-cert-hash )\K\S+"
  check_mode: false
  changed_when: false
  register: token_worker_hash

- name: display token
  when: inventory_hostname in [groups["master"][0]]
  ansible.builtin.debug:
    var: token_worker["stdout"]

- name: display token hash
  when: inventory_hostname in [groups["master"][0]]
  ansible.builtin.debug:
    var: token_worker_hash["stdout"]

- name: assign token vars to plain vars on worker nodes
  when: inventory_hostname in groups["slave"]
  set_fact:
    ktoken:      "{{ hostvars[groups['master'][0]]['token_worker']['stdout']}}"
    ktoken_hash: "{{ hostvars[groups['master'][0]]['token_worker_hash']['stdout']}}"

- name: display certificate key
  when: inventory_hostname in [groups["master"][0]]
  ansible.builtin.debug:
    var: certificate_key["stdout"]

# GETTING CONTROL PLANE TOKENS

- name: get control plane join command
  when: inventory_hostname in [groups["master"][0]]
  ansible.builtin.shell:
    cmd: "/usr/bin/kubeadm token create --print-join-command --certificate-key {{ certificate_key['stdout'] }}"
  register: control_join
  until: control_join.rc == 0
  retries: 20
  delay: 20

- name: extract control token
  when: inventory_hostname in [groups["master"][0]]
  shell: |
    echo "{{ control_join['stdout'] }}" | grep -o -P "(--token )\K\S+"
  check_mode: false
  changed_when: false
  register: token_control

- name: extract control token-cert-hash
  when: inventory_hostname in [groups["master"][0]]
  shell: |
    echo "{{ control_join['stdout'] }}" | grep -o -P "(--discovery-token-ca-cert-hash )\K\S+"
  check_mode: false
  changed_when: false
  register: token_control_hash

- name: display token
  when: inventory_hostname in [groups["master"][0]]
  ansible.builtin.debug:
    var: token_control["stdout"]

- name: display token hash
  when: inventory_hostname in [groups["master"][0]]
  ansible.builtin.debug:
    var: token_control_hash["stdout"]

# be aware the same var will resolve to different things depending on host
# worker token will not be the same as the token variable found on the masters
- name: assign token vars to plain vars on control nodes
  when: inventory_hostname in groups["master"]
  set_fact:
    ktoken:      "{{ hostvars[groups['master'][0]]['token_control']['stdout']}}"
    ktoken_hash: "{{ hostvars[groups['master'][0]]['token_control_hash']['stdout']}}"

# TURNING TOKENS INTO KUBE CONFIG

- name: overwrite our custom kubeadm config
  ansible.builtin.template:
    src:  "{{ kubeadm.config.src }}"
    dest: "{{ kubeadm.config.dest }}"
    trim_blocks: no
    owner: root
    group: root
    mode: '0644'
    backup: yes

# - name: save worker join to file
#   when: inventory_hostname in groups["slave"]
#   ansible.builtin.copy:
#     content: "#!/usr/bin/env bash\n{{ hostvars[groups['master'][0]]['worker_join']['stdout'] }} --config={{ kubeadm.config.dest }} --v 2 > /root/worker-join.log"
#     dest: "/root/worker-join.sh"
#     mode: '0700'
#
# # TODO: control join needs additional variables to offset the served port as conflicts with haproxy
# - name: save control plane join to file
#   when: inventory_hostname in groups["master"] and inventory_hostname not in [groups["master"][0]]
#   ansible.builtin.copy:
#     content: "#!/usr/bin/env bash\n{{ hostvars[groups['master'][0]]['control_join']['stdout'] }} --config={{ kubeadm.config.dest }} --v 2  > /root/control-join.log"
#     dest: "/root/control-join.sh"
#     mode: '0700'

- name: save join to file
  ansible.builtin.copy:
    content: "#!/usr/bin/env bash\nkubeadm join {{ hostvars[groups['master'][0]]['lb_ip'] }} --config={{ kubeadm.config.dest }} --v 2 > /root/join.log"
    dest: "/root/join.sh"
    mode: '0700'

# JOINING MASTERS

# - name: print shared control join command
#   when: inventory_hostname in groups["master"] and inventory_hostname not in [groups["master"][0]]
#   ansible.builtin.debug:
#     var: hostvars[groups["master"][0]]["control_join"]["stdout"]

- name: add remaining control plane nodes to kubernetes
  when: inventory_hostname in groups["master"] and inventory_hostname not in [groups["master"][0]]
  ansible.builtin.shell:
    cmd: /usr/bin/bash /root/join.sh
  register: result
  until: result.rc == 0
  retries: 20
  delay: 20

- name: copy secondary config
  when: inventory_hostname in groups["master"] and inventory_hostname not in [groups["master"][0]]
  copy:
    src: /etc/kubernetes/admin.conf
    dest: ${HOME}/.kube/config
    remote_src: yes
    mode: '0600'

# JOINING WORKERS

# - name: print shared worker join command
#   when: inventory_hostname in groups["slave"]
#   ansible.builtin.debug:
#     var: hostvars[groups["master"][0]]["worker_join"]["stdout"]

- name: add remaining worker nodes to kubernetes
  when: inventory_hostname in groups["slave"]
  ansible.builtin.shell:
    cmd: /usr/bin/bash /root/join.sh
  register: result
  until: result.rc == 0
  retries: 20
  delay: 20

# TODO: FIX
# error execution phase preflight: [preflight] Some fatal errors occurred:
# 	[ERROR FileContent--proc-sys-net-bridge-bridge-nf-call-iptables]: /proc/sys/net/bridge/bridge-nf-call-iptables does not exist
# [preflight] If you know what you are doing, you can make a check non-fatal with `--ignore-preflight-errors=...`
# To see the stack trace of this error execute with --v=5 or higher
# SOLUTION: modprobe br_netfilter to create virtual /proc space
# https://stackoverflow.com/q/62827781/11164973

# - name: finalise nodes
#   hosts: node
#   remote_user: root
#   tasks:

- name: kubelet config cgroups -> systemd
  ansible.builtin.lineinfile:
    path: /var/lib/kubelet/config.yaml
    regexp: '^cgroupDriver:'
    line: 'cgroupDriver: systemd'

- name: enable kubelet
  ansible.builtin.systemd:
    name: kubelet
    state: started
    enabled: yes
