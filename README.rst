DeepCypher Collection of Reusable Ansible Roles
===============================================

This collection of Ansible roles focuses on provisioning of multi-architecture high availability Kubernetes infrastructure on bare metal clusters.

Here is a brief overview of the intended order and functionality of the roles:

- Partition; Declarative partitioning of all drives across all machines.
- RAID; Declarative (lvm) RAID across drives on each machine.
- Linux; Archlinux pacstrapping and initial bootstrapping on arbitrary dev block.
- Boot; Grub based booting with support for RAID bios and uefi.
- Kubernetes; Declarative Kubernetes cluster bootstrapping, destruction and maintenance.

There is no variable passing between the above roles. They are intended to be able to run independently. However since the structure of your cluster is declarative each role knows what its jobs are. E.G the RAID role has no concept of how to partition or what indeed is patitioned. You simply tell it what partitions you want RAIDed together and it will do that if it can. Clearly this means to ensure the partitions exist you should probably execute the partition role first. However this means you can go and manually partition how you like then call RAID specifying the partitions it should use.
